import React, { Component } from "react";

class DefaultFooter extends Component {
	render() {
		return (
			<React.Fragment>
				<span><a href="//instagram.com/tifuinsuka16" target="_blank" rel="noopener noreferrer">Teknik Informatika</a> &copy; UIN Sunan Kalijaga</span>
				<span className="ml-auto">Made with <i className="fa icon-heart" style={{ color: "red" }}></i> by <a href="//facebook.com/tegal6etar">Alvriyanto Azis</a></span>
			</React.Fragment>
		);
	}
}

export default DefaultFooter;
