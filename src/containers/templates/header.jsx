import React, { Component } from "react";
import { Badge, UncontrolledDropdown, DropdownItem, DropdownMenu, Nav, NavItem } from "reactstrap";

// import { /* AppAsideToggler, */ AppNavbarBrand } from "@coreui/react";
// import logo from "../../assets/img/logo/logo.png";
// import logo_circle from "../../assets/img/logo/logo.png";

class Header extends Component {
	render() {
		return (
			<React.Fragment>
				{/* <AppNavbarBrand
					full={{ src: logo, width: 89, height: 25, alt: 'Document Search Engine' }}
					minimized={{ src: logo_circle, width: 30, height: 30, alt: 'Document Search Engine' }}
					/> */}
				<Nav className="d-md-down-none" navbar>
					<NavItem className="px-3">
						<a href="//digilib.uin-suka.ac.id/information.html" className="nav-link">About</a>
					</NavItem>
					<NavItem className="px-3">
						<a href="//digilib.uin-suka.ac.id/cgi/latest" className="nav-link">Latest Addition</a>
					</NavItem>
					<NavItem className="px-3">
						<a href="//digilib.uin-suka.ac.id/cgi/search/advanced" className="nav-link">Advanced Search</a>
					</NavItem>
					<NavItem className="px-3">
						<a href="//digilib.uin-suka.ac.id/view" className="nav-link">Browse</a>
					</NavItem>

					<NavItem className="px-3">
						<a href="//digilib.uin-suka.ac.id/view/year" className="nav-link">Policy</a>
					</NavItem>
					<NavItem className="px-3">
						<a href="//digilib.uin-suka.ac.id/19739" className="nav-link">Upload Procedure</a>
					</NavItem>
					<NavItem className="px-3">
						<a href="//digilib.uin-suka.ac.id/faq.html" className="nav-link">FAQ</a>
					</NavItem>
					<NavItem className="px-3">
						<a href="//digilib.uin-suka.ac.id/cgi/stats/report" className="nav-link">Statistics</a>
					</NavItem>
				</Nav>
				<Nav className="ml-auto" navbar>
					<UncontrolledDropdown nav direction="down">
						<DropdownMenu right>
							<DropdownItem header tag="div" className="text-center"><strong>Account</strong></DropdownItem>
							<DropdownItem><i className="fa fa-bell-o"></i> Updates<Badge color="info">42</Badge></DropdownItem>
							<DropdownItem><i className="fa fa-envelope-o"></i> Messages<Badge color="success">42</Badge></DropdownItem>
							<DropdownItem><i className="fa fa-tasks"></i> Tasks<Badge color="danger">42</Badge></DropdownItem>
							<DropdownItem><i className="fa fa-comments"></i> Comments<Badge color="warning">42</Badge></DropdownItem>
							<DropdownItem header tag="div" className="text-center"><strong>Settings</strong></DropdownItem>
							<DropdownItem><i className="fa fa-user"></i> Profile</DropdownItem>
							<DropdownItem><i className="fa fa-wrench"></i> Settings</DropdownItem>
							<DropdownItem><i className="fa fa-usd"></i> Payments<Badge color="secondary">42</Badge></DropdownItem>
							<DropdownItem><i className="fa fa-file"></i> Projects<Badge color="primary">42</Badge></DropdownItem>
							<DropdownItem divider />
							<DropdownItem><i className="fa fa-shield"></i> Lock Account</DropdownItem>
							<DropdownItem onClick={e => this.props.onLogout(e)}><i className="fa fa-lock"></i> Logout</DropdownItem>
						</DropdownMenu>
					</UncontrolledDropdown>
				</Nav>
				{/* <AppAsideToggler mobile /> */}
			</React.Fragment>
		);
	}
}

export default Header;