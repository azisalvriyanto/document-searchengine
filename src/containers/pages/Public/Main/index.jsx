import React, { Component }	from "react";
import {
	//page
	Col,
	Card,
	CardHeader,
	CardBody,
	Row,

	//navbar
	// Nav,
	// NavItem,
	// NavLink,
	// TabContent,
	// TabPane,

	//input
	Input,
	InputGroup,
	InputGroupAddon,
	InputGroupText,
}	from "reactstrap";
// import classNames	from "classnames";

import {
	Button,
	// Chart,
	Table
}	from "../../../../components/molecules";

import Stopwords	from "../../../organisms/Stopwords";
import sastrawi		from "sastrawijs";

import { API }		from "../../../../config/api";
import { connect }	from "react-redux";
import {
	getState,
	getFirebaseData,
	updateFirebaseData,
	getUpdateAt,
	updateUpdateAt
}	from "../../../../config/redux/action";

// import Database	from "../../../organisms/Database";

class Main extends Component {
	state = {
		database	: {},
		calculation	: {},
		query		: {},
		data		: {},
		time		: {},
		vsm			: {},
		result		: [],
		isLoading	: false,
		activeTab	: new Array(2).fill("1"),
		information	: "",
		update_at	: ""
	}

	handleTerm		= text		=> {
		const tokenizer	= new sastrawi.Tokenizer();
		const stemmer	= new sastrawi.Stemmer();
		const stopwords	= Stopwords();
		const breakers	= ["\n", "", ".", ",", "'", "\"", ":", ";", "?", "\r\n", "!", "--", "-", "(", ")", "\r\n\r\n", "\r\n\r\n\r\n", "]", "["];

		const words	= tokenizer.tokenize(text);
		const tf	= [];
		for (const word of words) {
			const term	= stemmer.stem(word);
			if (
				term !== ""
				&& !stopwords.includes(term)	=== true
				&& !breakers.includes(term)		=== true
			) {
				tf[term]	= !isNaN(tf[term]) ? tf[term]+1 : 1;
			}
		}

		return tf;
	}

	handleSubmit	= async ()	=> {
		const state	= this.state
		this.setState({
			data		: {},
			time		: {},
			result		: [],
			isLoading	: true,
			information	: ""
		});

		const time		= [];
		time["start"]	= Date.now();

		const query		= this.handleTerm(state.query.text);
		const search	= await API.search(Object.keys(query).join(" "));

		if (search.meta.code === 200) {
			const document = search.data;
			this.setState({
				data: document,
			});

			const pow		= [];
			const ps		= [];
			pow["query"]	= [];
			ps["query"]		= [];
			const tfidf		= [];
			for (const k_query in query) {
				if (typeof state.calculation[k_query] !== "object") {
					this.setState({
						calculation: {
							...state.calculation,
							[k_query]: {
								df: 0,
								idf: 0
							}
						}
					});
				}

				tfidf[k_query]	= query[k_query]*(typeof state.calculation[k_query] === "object" ? state.calculation[k_query].idf : 0);

				pow["query"][k_query]	= Math.pow(tfidf[k_query], 2);
				ps["query"]["pow"]		= (!isNaN(ps["query"]["pow"]) ? ps["query"]["pow"] : 0) + pow["query"][k_query];
			}
			ps["query"]["sqrt"]	= Math.sqrt(ps["query"]["pow"]);

			this.setState({
				query: {
					text: state.query.text,
					tf: query,
					tfidf: tfidf
				}
			});

			const vsm		= [];
			vsm["query"]	= {
				pow: pow["query"],
				ps: ps["query"]
			};

			let document_id 	= 0;
			const qd			= [];
			vsm["calculation"]	= [];
            const vsm_temp		= [];
			for (const k_document in document) {
				document_id = document[k_document].id;

				qd[document_id]							= [];
                qd[document_id]["query"]				= [];

				for (const k_query in query) {                    
					qd[document_id]["query"][k_query] 	= tfidf[k_query] * (typeof state.database[document_id].term[k_query] === "object" ? state.database[document_id].term[k_query].tfidf : 0);
					qd[document_id]["total"]			= (!isNaN(qd[document_id]["total"]) ? qd[document_id]["total"] : 0) + qd[document_id]["query"][k_query];
				}

				vsm["calculation"][document_id]			= [];
				vsm["calculation"][document_id]["id"]	= parseInt(document[k_document].id);
				vsm["calculation"][document_id]["qd"]	= qd[document_id]["query"];
				vsm["calculation"][document_id]["vsm"]	= (qd[document_id]["total"]/(ps["query"]["sqrt"]*state.database[document_id].sqrt)) || 0;

				//variabel of sequence
				vsm_temp[document_id]				= [];
				vsm_temp[document_id]["id"]			= vsm["calculation"][document_id]["id"];
				vsm_temp[document_id]["title"]		= document[k_document].title;
				vsm_temp[document_id]["year"]		= document[k_document].year;
				vsm_temp[document_id]["creator"]	= document[k_document].creator;
				vsm_temp[document_id]["uri"]		= document[k_document].uri;
				vsm_temp[document_id]["vsm"]		= vsm["calculation"][document_id]["vsm"];
			}

			this.setState({
				vsm: {
					...state.vsm,
					query: vsm["query"],
					calculation: vsm["calculation"]
				},
				result: vsm_temp
			});
		}

		time["end"]			= Date.now();
		time["difference"]	= (time.end - time.start) / 1000;

		this.setState({
			time: time,
			isLoading: false
		});
	}

	handleInput		= e			=> {
		this.setState({
			query: {
				...this.state.query,
				text: e.target.value
			}
		});
	}

	handleEnterPress	= e		=> {
		if(e.keyCode === 13 && e.shiftKey === false) {
			e.preventDefault();
			this.handleSubmit();
		}
	}

	handleToggle	= (tabPane, tab)	=> {
		const newArray		= this.state.activeTab.slice();
		newArray[tabPane]	= tab;
		this.setState({
			activeTab: newArray
		});
	}

	componentDidMount	= async ()		=> {
		document.title = "Document Search Engine uses the Vector Space Model Algorithms";

		const {
			getState,
			getUpdateAt,
			updateUpdateAt
		}	= this.props;

		this.setState({
			...this.state,
			isLoading	: true,
		});

		const apiUpdateAt		= await API.update_at();
		const storageUpdateAt	= await getUpdateAt();
		if (
			storageUpdateAt.status		=== true
			&& apiUpdateAt.meta.code	=== 200
		) {
			const { update_at }	= this.props;
			if (new Date(apiUpdateAt.data) > new Date(update_at)) {
				console.log("Mengambil data dari database.");
				const database	= await API.document();
				if (database.meta.code === 200) {
					console.log("Menghitung tf, dan df setiap term.");
					const data	= [];
					const df	= [];
					const idf	= [];
					for (let index in database.data) {
						data[database.data[index].id] = {
							id: database.data[index].id,
							creator: database.data[index].creator,
							title: database.data[index].title,
							publication: database.data[index].publication,
							uri: database.data[index].uri,
							tf: this.handleTerm(
								database.data[index].creator + " " +
								database.data[index].title + " " +
								database.data[index].publication + " " +
								database.data[index].keywords + " " +
								database.data[index].abstract + " " +
								database.data[index].text
							)
						}
	
						for (let jndex in data[database.data[index].id].tf) {
							df[jndex] = !isNaN(df[jndex]) ? df[jndex] + 1 : 1;
						}
					}

					console.log("Menghitung idf setiap term.");
					const temp_calculation = [];
					for (let jndex in df) {
						let idf_value 	= Math.log10(Object.keys(data).length / df[jndex]);
						idf[jndex]		= !isNaN(idf_value) ? idf_value : 0;

						temp_calculation[jndex] = {
							df: df[jndex],
							idf: idf[jndex]
						};
					}

					console.log("Memasukan df dan idf ke Firebase.");
					try {
						await this.props.updateFirebaseData("state/calculation/", temp_calculation);
						console.log("    > Success");
					} catch (error) {
						console.error("    > Error:", error.message);
					}

					console.log("Menghitung TFIDF setiap term dan akar(E Wij^2) setiap dokumen.");
					const tfidf			= [];
					const pow			= [];
					const ps			= [];
					const temp_database	= [];
					// const state			= await this.props.getFirebaseData("state/database/");
					for (let index in data) {
						console.log(`    Document ${index}`);
						// if (typeof state.data[index] !== "object") {
							tfidf[index] = [];
							pow[index] = [];
							ps[index] = [];
							ps[index]["pow"] = 0;
	
							const temp_database_calculation = [];
							for (let kndex in data[index].tf) {
								tfidf[index][kndex] = data[index].tf[kndex] * idf[kndex];
								pow[index][kndex] = Math.pow(tfidf[index][kndex], 2);
								ps[index]["pow"] += pow[index][kndex];
	
								temp_database_calculation[kndex] = {
									tf: data[index].tf[kndex],
									tfidf: tfidf[index][kndex]
								}
							}
							ps[index]["sqrt"] = Math.sqrt(ps[index]["pow"]);
	
							data[index]["tfidf"] = tfidf[index];
	
							temp_database[index] = {
								term: temp_database_calculation,
								pow: ps[index]["pow"],
								sqrt: ps[index]["sqrt"]
							}
	
							try {
								await this.props.updateFirebaseData(`state/database/${index}/`, temp_database[index]);
								console.log(`        > Success`);
							} catch (error) {
								console.error(`        > Error:`, error.message);
							}
						// } else {
						// 	console.log("        > Already");
						// }
					}

					console.log("Update update_at.");
					try {
						await updateUpdateAt(apiUpdateAt.data);
						console.log("    > Success");
					} catch (error) {
						console.error("    > Error:", error.message);
					}
				}
				else {
					console.log("Error:", database.meta.message);
				}
			}

			console.log("Mencoba setState dari data Firebase.");
			const storageGetState	= await getState();
			if (storageGetState.status === true) {
				const { state }	= this.props;
				this.setState({
					...this.state,
					calculation	: state.calculation,
					database	: state.database,
					update_at	: state.update_at
				});
				console.log("Sukses setState dari data Firebase.");
			}
			else {
				alert("Connection is low, please reload this page.");
			}
		}
		else {
			alert("Connection is low, please reload this page.");
		}

		this.setState({
			...this.state,
			isLoading	: false,
		});
	}

	static getDerivedStateFromProps(props, state) {
		if (
			state.update_at !== ""
			&& new Date(props.update_at) > new Date(state.update_at)
		) {
			console.log("Data telah diperbarui.");
			return {	
				...state,
				calculation	: props.state.calculation,
				database	: props.state.database,
				vsm			: props.state.vsm,
				update_at	: props.update_at
			};
		}
		else {
			return null;
		}
	}

	render() {
		// const { database, query, data, calculation, vsm, result, isLoading, activeTab } = this.state;
		const {data, result, isLoading } = this.state;
		// console.log("render|state: ", this.state, "\n", "render|props: ", this.props);
		// console.log("render|state: ", this.state);

		return (
			<div className="animated fadeIn">
				<Row className="justify-content-center mt-5 mb-5">
					<Col md="12" lg="6">
						<div className="clearfix">
							<h1 className="float-left display-4 mr-2">
								Document
							</h1>
							<h4 className="pt-2">Search Engine</h4>
							<p className="float-left text-muted">uses the Vector Space Model Algorithms</p>
						</div>

						<InputGroup className="input-prepend">
							<InputGroupAddon addonType="prepend">
								<InputGroupText>
									<i className="fa fa-search"></i>
								</InputGroupText>
							</InputGroupAddon>
							<Input
								type="text"
								className="form-control-lg form-control-sm"
								placeholder="Enter search your keywords"
								onChange={ this.handleInput }
								onKeyDown={ this.handleEnterPress }
								disabled={ isLoading }
								/>
							<InputGroupAddon addonType="append">
								<Button
									onClick={ this.handleSubmit }
									isLoading={ isLoading }
									/>
							</InputGroupAddon>
						</InputGroup>

						{
							Object.keys(this.state.time).length === 3
							?	<div className="clearfix">
									<p className="text-muted mt-2 ml-1">
										About { Object.keys(data).length } results ({ this.state.time.difference } milliseconds)
									</p>
								</div>
							:	null
						}
					</Col>
				</Row>

				<Row>
					<Col>
						<Card>
							<CardHeader>
								<i className="fa icon-equalizer"></i>
								<strong>Result</strong>
							</CardHeader>
							<CardBody>
								{
									Object.keys(result).length > 0
									?	<Table
											type="result-vsm"
											thead={[
												[
													{ text: "#" },
													{ text: "Document" },
													{ text: "Year" },
													{ text: "Creator" },
													{ text: "Title" }
												]
											]}

											tbody={ result }
											/>
									:	<span>Data not found.</span>
								}
							</CardBody>
						</Card>
					</Col>
				</Row>
			</div>
		)
	}
}

const reduxState	= (state)		=> ({
	state		: state.state,
	update_at	: state.update_at
});

const reduxDispatch	= (dispatch)	=> ({
    getState		: ()		=> dispatch(getState()),
	getUpdateAt		: ()		=> dispatch(getUpdateAt()),
	updateUpdateAt	: (data)	=> dispatch(updateUpdateAt(data)),

	getFirebaseData		: (data)			=> dispatch(getFirebaseData(data)),
	updateFirebaseData	: (reference, data)	=> dispatch(updateFirebaseData(reference, data)),
});

export default connect(reduxState, reduxDispatch)(Main);