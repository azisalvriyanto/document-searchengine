import React, { Component, Suspense }	from "react";
import { Container }	from "reactstrap";

import {
	// AppAside,
	AppFooter,
	AppHeader
}	from "@coreui/react";

import "./index.scss";

const Header	= React.lazy(() => import("../../templates/header"));
const Footer	= React.lazy(() => import("../../templates/footer"));
// const Aside 	= React.lazy(() => import("../../templates/aside"));

const Main			= React.lazy(() => import("./Main"));
const Conventional	= React.lazy(() => import("./Conventional"));
const VSM			= React.lazy(() => import("./VSM"));

class Public extends Component {
	loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

	renderSwitch = (param) => {
		switch(param) {
			case "/":
				return <Main />
			case "/conventional":
				return <Conventional />
			case "/vsm":
				return <VSM />
			default:
			return null
		}
	}

	render() {
		return (
			<div className="app">
				<AppHeader fixed>
					<Suspense fallback={ this.loading() }>
						<Header />
					</Suspense>
				</AppHeader>
				<div className="app-body">
					<main className="main">
						<Container fluid>
							<Suspense fallback={this.loading()}>
								{ this.renderSwitch(this.props.location.pathname) }
							</Suspense>
						</Container>
					</main>

					{/* <AppAside fixed>
						<Suspense fallback={ this.loading() }>
							<Aside />
						</Suspense>
					</AppAside> */}
				</div>
				<AppFooter>
					<Suspense fallback={ this.loading() }>
						<Footer />
					</Suspense>
				</AppFooter>
			</div>
		)
	}
}

export default Public;