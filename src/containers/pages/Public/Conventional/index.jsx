import React, { Component } from "react";
import {
	//page
	Col,
	Card,
	CardHeader,
	CardBody,
	Row,

	//navbar

	//input
	Input,
	InputGroup,
	InputGroupAddon,
	InputGroupText,
}	from "reactstrap";

import {
	Button,
	Table
}	from "../../../../components/molecules";

import Stopwords	from "../../../organisms/Stopwords";
import sastrawi from "sastrawijs";

import { API }		from "../../../../config/api";

class Conventional extends Component {
    state = {
        query		: {},
		data		: {},
        time		: {},
		isLoading	: false,
		result		: [],
		information	: "",
		orderby		: "year"
    }

    handleTerm		= text		=> {
		const tokenizer	= new sastrawi.Tokenizer();
		const stemmer	= new sastrawi.Stemmer();
		const stopwords	= Stopwords();
		const breakers	= ["\n", "", ".", ",", "'", "\"", ":", ";", "?", "\r\n", "!", "--", "-", "(", ")", "\r\n\r\n", "\r\n\r\n\r\n", "]", "["];

		const words	= tokenizer.tokenize(text);
		const tf	= [];
		for (const word of words) {
			const term	= stemmer.stem(word);
			if (
				term !== ""
				&& !stopwords.includes(term)	=== true
				&& !breakers.includes(term)		=== true
			) {
				tf[term]	= tf[term] !== undefined ? tf[term]+1 : 1;
			}
		}

		return tf;
	}

	handleSubmit	= async ()	=> {
		const state	= this.state
		this.setState({
			data		: {},
			database	: {},
			time		: {},
			result		: [],
			isLoading	: true,
			information	: ""
		});

		const time		= [];
		time["start"]	= Date.now();

		const query		= this.handleTerm(state.query.text);
		const search	= await API.search(Object.keys(query).join(" "), state.orderby);

		if (search.meta.code === 200) {
			const document = search.data;
			this.setState({
				data	: document
			});
		}

		time["end"]			= Date.now();
		time["difference"]	= (time.end - time.start) / 1000;

		this.setState({
			time: time,
			isLoading: false
		});
	}

	handleInput		= e			=> {
		this.setState({
			query: {
				...this.state.query,
				text: e.target.value
			}
		});
	}

	 handleChange = (e) => {
		this.setState({
		...this.state,
		orderby: e.target.value,
		});
	};

	handleEnterPress	= e		=> {
		if(e.keyCode === 13 && e.shiftKey === false) {
			e.preventDefault();
			this.handleSubmit();
		}
    }
    
    componentDidMount() {
		document.title = "Document Search Engine uses the Conventional Algorithms";
    }

    render() {
        const { data, isLoading } = this.state;
		//console.log("render|state: ", this.state, "\n", "render|props: ", this.props);
		//console.log("render|state: ", this.state);

        return (
			<div className="animated fadeIn">
				<Row className="justify-content-center mt-5 mb-5">
					<Col md="12" lg="6">
						<div className="clearfix">
							<h1 className="float-left display-4 mr-2">
								Document
							</h1>
							<h4 className="pt-2">Search Engine</h4>
							<p className="float-left text-muted">uses the Conventional Algorithms</p>
						</div>

						<InputGroup className="input-prepend">
							<InputGroupAddon addonType="prepend">
								<InputGroupText>
									<select
									className="form-control form-control-sm"
									onChange={this.handleChange}
									value={this.state.orderby}
									>
										<option value="creator">Creator</option>
										<option value="year">Year</option>
										<option value="title">Title</option>
									</select>
								</InputGroupText>
							</InputGroupAddon>
							<Input
								type="text"
								className="form-control-lg form-control-sm"
								placeholder="Enter search your keywords"
								onChange={ this.handleInput }
								onKeyDown={ this.handleEnterPress }
								disabled={ isLoading }
								/>
							<InputGroupAddon addonType="append">
								<Button
									onClick={ this.handleSubmit }
									isLoading={ isLoading }
									/>
							</InputGroupAddon>
						</InputGroup>

						{
							Object.keys(this.state.time).length === 3
							?	<div className="clearfix">
									<p className="text-muted mt-2 ml-1">
										About { Object.keys(data).length } results ({ this.state.time.difference } milliseconds)
									</p>
								</div>
							:	null
						}
					</Col>
				</Row>

				<Row>
					<Col>
						<Card>
							<CardHeader>
								<i className="fa icon-docs"></i>
								<strong>Document</strong>
							</CardHeader>
							<CardBody>
								{
									Object.keys(data).length > 0
									?	<Table
											type="document"
											thead={[
												[
													{ text: "#" },
													{ text: "Document" },
													{ text: "Year" },
													{ text: "Creator" },
													{ text: "Title" }
												]
											]}

											tbody={ data }
											/>
									:	<span>Data not found.</span>
								}
							</CardBody>
						</Card>
					</Col>
				</Row>
			</div>
		)
	}
}

export default Conventional;