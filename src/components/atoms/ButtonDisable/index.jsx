import React		from "react";
import { Button }	from "reactstrap";

const ButtonDisable = props => {
	return <Button color={ props.color } block disabled>{ props.title }</Button>
}

export default ButtonDisable;