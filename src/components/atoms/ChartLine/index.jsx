import React				from "react";
import { Line }				from "react-chartjs-2";
import { CustomTooltips }	from "@coreui/coreui-plugin-chartjs-custom-tooltips";
import { getStyle }			from "@coreui/coreui/dist/js/coreui-utilities";

const brand = [
	getStyle("--primary"),
	getStyle("--success"),
	getStyle("--info"),
	getStyle("--warning"),
	getStyle("--danger")
]

const ChartLine = props => {
	const arr_y 	= [];
	const dataset	= [];
	let max_y		= 0;
	let i			= 0;
	for (const k_y in props.y) {
		arr_y[props.y[k_y]]	= [];
		for (const k_x in props.x) {
			arr_y[props.y[k_y]].push(!isNaN(props.x[k_x].value[props.y[k_y]]) ? props.x[k_x].value[props.y[k_y]] : 0);

			if (max_y < props.x[k_x].value[props.y[k_y]]) {
				max_y = props.x[k_x].value[props.y[k_y]];
			}
		}

		dataset.push({
			label: props.y[k_y],
			fill: false,
			backgroundColor: "rgba(75, 192, 192, 0.4)",
			borderWidth: 2,
			borderColor: brand[i],
			pointHoverBackgroundColor: "#FFFFFF",
			lineTension: 0.1,
			data: arr_y[props.y[k_y]]
		});

		if (i === 4) {
			i = 0;
		}
		else {
			i++;
		}
	}

	const line = {
		labels		: props.x.map(k_x => {
			return k_x.name
		}),
		datasets	: dataset
	};

	const options = {
		tooltips: {
			enabled: false,
			custom: CustomTooltips,
			intersect: true,
			mode: "index",
			position: "nearest",
			callbacks: {
				labelColor: function(tooltipItem, chart) {
					return { backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor }
				}
			}
		},
		maintainAspectRatio: false,
		legend: {
			display: false,
		},
		elements: {
			point: {
				radius: 2,
				hitRadius: 10,
				hoverRadius: 4,
				hoverBorderWidth: 3,
			}
		}
	}

	return	<Line data={ line } options={ options } />
}

export default ChartLine;