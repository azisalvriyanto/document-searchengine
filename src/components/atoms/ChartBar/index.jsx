import React				from "react";
import { Bar }				from "react-chartjs-2";
import { CustomTooltips }	from "@coreui/coreui-plugin-chartjs-custom-tooltips";
import { getStyle }			from "@coreui/coreui/dist/js/coreui-utilities";

const brand = [
	getStyle("--primary"),
	getStyle("--success"),
	getStyle("--info"),
	getStyle("--warning"),
	getStyle("--danger")
];

const ChartBar = props => {
	const arr_y 	= [];
	const dataset	= [];
	let max_y		= 0;
	let i			= 0;
	for (const k_y in props.y) {
		arr_y[props.y[k_y]]	= [];
		for (const k_x in props.x) {
			arr_y[props.y[k_y]].push(!isNaN(props.x[k_x].value[props.y[k_y]]) ? props.x[k_x].value[props.y[k_y]] : 0);

			if (max_y < props.x[k_x].value[props.y[k_y]]) {
				max_y = props.x[k_x].value[props.y[k_y]];
			}
		}

		dataset.push({
			label: props.y[k_y],
			backgroundColor: "transparent",
			borderColor: brand[i],
			pointHoverBackgroundColor: "#fff",
			borderWidth: 2,
			data: arr_y[props.y[k_y]],
		});

		if (i === 4) {
			i = 0;
		}
		else {
			i++;
		}
	}

	const bar		= {
		labels		: props.x.map(k_x => {
			return k_x.name
		}),
		datasets	: dataset
	};

	const options	= {
		tooltips: {
			enabled: false,
			custom: CustomTooltips
		},
		maintainAspectRatio: false
	}

	return	<Bar data={ bar } options={ options } />
}

export default ChartBar;