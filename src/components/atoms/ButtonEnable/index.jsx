import React		from "react";
import { Button }	from "reactstrap";

const ButtonEnable = props => {
	return <Button color={ props.color } onClick={ props.onClick }>{ props.title }</Button>
}

export default ButtonEnable;