import React		from "react";
import { Table }	from "reactstrap";

const TableStriped = props => {
	return <Table responsive striped>
				<thead>
					{
						props.thead.map((tr, k_tr) => {
							const th = [];
							for (const k_th in tr) {
								th.push(<th
											key={ k_th }
											colSpan={ "colSpan" in tr[k_th] ? tr[k_th].colSpan : 1 }
											rowSpan={ "rowSpan" in tr[k_th] ? tr[k_th].rowSpan : 1 }
											>
											{ tr[k_th].text }
										</th>)
							}

							return	<tr key={ k_tr }>
										{ th }
									</tr>
						})
					}
				</thead>
				<tbody>
					{
						props.tbody.map((tr, k_tr) => {
							const td = [];
							for (const k_td in tr) {
								td.push(<td key={ k_td }>{ tr[k_td].text }</td>)
							}

							return	<tr key={ k_tr }>
										{ td }
									</tr>
						})
					}
				</tbody>
				{
					"tfoot" in props ?
						<tfoot>{ props.tfoot.map((tr, k_tr) => {
							const th = [];
							for (const k_th in tr) {
								th.push(<th
											key={ k_th }
											colSpan={ "colSpan" in tr[k_th] ? tr[k_th].colSpan : 1 }
											rowSpan={ "rowSpan" in tr[k_th] ? tr[k_th].rowSpan : 1 }
											>
											{ tr[k_th].text }
										</th>)
							}

							return	<tr key={ k_tr }>
										{ th }
									</tr>
						}) }</tfoot>
						: null
				}
			</Table>
}

export default TableStriped;