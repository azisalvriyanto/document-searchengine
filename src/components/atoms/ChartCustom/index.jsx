import React				from "react";
import { Line }				from "react-chartjs-2";
import { CustomTooltips }	from "@coreui/coreui-plugin-chartjs-custom-tooltips";
import { getStyle }			from "@coreui/coreui/dist/js/coreui-utilities";

const brand = [
	getStyle("--primary"),
	getStyle("--success"),
	getStyle("--info"),
	getStyle("--warning"),
	getStyle("--danger")
]

const ChartCustom = props => {
	const arr_y 	= [];
	const dataset	= [];
	let max_y		= 0;
	let i			= 0;
	for (const k_y in props.y) {
		arr_y[props.y[k_y]]	= [];
		for (const k_x in props.x) {
			arr_y[props.y[k_y]].push(typeof props.x[k_x].value[props.y[k_y]] === "object" ? props.x[k_x].value[props.y[k_y]].tfidf : 0);

			if (
				typeof props.x[k_x].value[props.y[k_y]] === "object"
				&& max_y < props.x[k_x].value[props.y[k_y]].tfidf) {
				max_y = props.x[k_x].value[props.y[k_y]].tfidf;
			}
		}

		dataset.push({
			label: props.y[k_y],
			fill: false,
			backgroundColor: "rgba(75, 192, 192, 0.4)",
			borderColor: brand[i],
			pointHoverBackgroundColor: "#FFFFFF",
			borderWidth: 2,
			data: arr_y[props.y[k_y]],
		});

		if (i === 4) {
			i = 0;
		}
		else {
			i++;
		}
	}

	const mainChart = {
		labels		: props.x.map(k_x => {
			return k_x.name
		}),
		datasets	: dataset
	};

	const Option = {
		tooltips: {
			enabled: false,
			custom: CustomTooltips,
			intersect: true,
			mode: "index",
			position: "nearest",
			callbacks: {
				labelColor: function(tooltipItem, chart) {
					return { backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor }
				}
			}
		},
		maintainAspectRatio: false,
		elements: {
			point: {
				radius: 2,
				hitRadius: 10,
				hoverRadius: 4,
				hoverBorderWidth: 3,
			}
		}
	};

	return	<Line data={ mainChart } options={ Option } height={ 300 } />
}

export default ChartCustom;