import React	from "react";
import {
	TableHover,
	TableStriped
}	from "../../atoms";

import {
	Badge
} from "reactstrap";

const multisort	= (array, columns, order) => {
	return array.sort(function (a, b) {
		const direction = order === "DESC" ? 1 : 0;

		const is_numeric = !isNaN(+a[columns] - +b[columns]);

		const x = is_numeric ? +a[columns] : a[columns];
		const y = is_numeric ? +b[columns] : b[columns];

		if(x < y) {
			return direction === 0 ? -1 : 1;
		}

		return direction === 0 ? 1 : -1;
	});
}

const Table = (props) => {
	if (props.type === "document") {
		const tbody = Object.keys(props.tbody).map((k_document, key) => {
			return [
				{ text: `${key + 1}` },
				{ text: <a href={props.tbody[k_document].uri} target="_blank" rel="noopener noreferrer">doc[{props.tbody[k_document].id}]</a> },
				{ text: props.tbody[k_document].year },
				{ text: props.tbody[k_document].creator },
				{ text: props.tbody[k_document].title }
			]
		});

		return <TableHover
			thead={ props.thead }
			tbody={ tbody }
		/>
	}
	else if (props.type === "tfidf") {
		const tbody = Object.keys(props.tbody).map((k_document, id) => {
			const document_id 	= props.tbody[k_document].id;

			const td			= [];
			td.push({ text: `doc[${ document_id }]` });

			for (const k_query in props.etc.query.tf) {
				td.push({
					text: typeof props.etc.database[document_id].term[k_query] === "object"
					? props.etc.database[document_id].term[k_query].tf
					: 0
				})
			}

			for (const k_query in props.etc.query.tf) {
				td.push({
					text: <Badge color="success">{ typeof props.etc.database[document_id].term[k_query] === "object"
					? props.etc.database[document_id].term[k_query].tfidf
					: 0 }</Badge>
				})
			}

			return td;
		});

		const tfoot	= [
						[].concat.apply([],
							[
								{
									text: "DF"
								},
								Object.keys(props.etc.query.tf).map((k_query, key) => {
									return {
										text: <Badge color="danger">{ props.etc.calculation[k_query].df }</Badge>
									}
								}),
								{
									text: "",
									colSpan: Object.keys(props.etc.query.tf).length
								}
							]
						),
						[].concat.apply([],
							[
								{
									text: "IDF"
								},
								Object.keys(props.etc.query.tf).map((k_query, key) => {
									return {
										text: <Badge color="warning">{ props.etc.calculation[k_query].idf }</Badge>
									}
								}),
								{
									text: "",
									colSpan: Object.keys(props.etc.query.tf).length
								}
							]
						),
						
					];

		return <TableStriped
			thead={ props.thead }
			tbody={ tbody }
			tfoot={ tfoot }
		/>
	}
	else if (props.type === "vsm") {
		const tbody = Object.keys(props.tbody.calculation).map((k_document, id) => {
			const td		= [];
			td.push({ text: `doc[${ k_document }]` });
			for (const k_query in props.etc.query.tf) {
				td.push({ text: props.tbody.calculation[k_document].qd[k_query] });
			}

			td.push(
				{ text: <Badge color="info">{ props.etc.database[k_document].pow }</Badge> },
				{ text: <Badge color="success">{ props.tbody.calculation[k_document].vsm }</Badge> }
			);

			return td;
		});

		const tfoot = [
						[].concat.apply([],
							[
								{
									text: "Query[j]^2"
								},
								Object.keys(props.etc.query.tf).map((k_query, id) => {
									return {
										text: <Badge color="info">{ props.tbody.query.pow[k_query] }</Badge>
									}
								}),
								{
									text: "",
									colSpan: 2
								}
							]
						)
					];

		return <TableStriped
			thead={ props.thead }
			tbody={ tbody }
			tfoot={ tfoot }
		/>
	}
	else if (props.type === "result") {
		const tbody	= multisort(props.tbody, "vsm", "DESC").map((result, key) => {
			return [
				{ text: `${ key+1}` },
				{ text: result.vsm.toFixed(5) },
				{ text: <a href={ result.uri } target="_blank" rel="noopener noreferrer">doc[{ result.id }]</a> },
				{ text: result.creator },
				{ text: result.title }
			]
		});

		return <TableHover
			thead={ props.thead }
			tbody={ tbody }
		/>
	}
	else if (props.type === "result-vsm") {
		const tbody	= multisort(props.tbody, "vsm", "DESC").map((result, key) => {
			return [
				{ text: `${ key+1}` },
				{ text: <a href={ result.uri } target="_blank" rel="noopener noreferrer">doc[{ result.id }]</a> },
				{ text: result.year },
				{ text: result.creator },
				{ text: result.title }
			]
		});

		return <TableHover
			thead={ props.thead }
			tbody={ tbody }
		/>
	}
	else {
		return null;
	}
}

export default Table;