import React		from "react";
import {
	ButtonDisable,
	ButtonEnable
}	from "../../atoms";

const Button = props => {
	if(props.isLoading) {
		return <ButtonDisable
					title="Loading..."
					color="primary"
					/>
	}
	else {
		return <ButtonEnable
					title="Search"
					color="info"
					onClick={ props.onClick }
					/>
	}
}

export default Button;