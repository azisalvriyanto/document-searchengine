import React	from "react";
import {
	ChartBar,
	ChartCustom,
	ChartLine
}	from "../../atoms";

const Chart = props => {
	return	<div className="chart-wrapper" style={{ height: 300 + "px" }}>
				{
					(() => {
						if		(props.type === "bar") {
							return	<ChartBar
										x={ props.x }
										y={ props.y }
										title={ props.title }
										etc={ props.etc }
										/>
						}
						else if	(props.type === "line") {
							return	<ChartLine
										x={ props.x }
										y={ props.y }
										title={ props.title }
										etc={ props.etc }
										/>
						}
						else	{
							return	<ChartCustom
										x={ props.x }
										y={ props.y }
										title={ props.title }
										etc={ props.etc }
										/>
						}
					}) ()
				}
			</div>
}

export default Chart;