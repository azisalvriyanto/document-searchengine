import React		from "react";
import {
	BrowserRouter as Router,
	Route,
	Switch
}	from "react-router-dom";

import { Provider }	from "react-redux";
import { store }	from "../redux";

import Public		from "../../containers/pages/Public";

export const App	= ()	=> {
	return (
		<Provider store={ store }>
			<Router>
				<Switch>
					<Route exact path="/" component={ Public } />
					<Route exact path="/conventional" component={ Public } />
					<Route exact path="/vsm" component={ Public } />
				</Switch>
			</Router>
		</Provider>
	)
}