import firebase	from "firebase/app";
import "firebase/database";

const firebaseConfig = {
    apiKey: "AIzaSyBQYq3rMPv-toWljFbVBaRw4h0wl1dM7d4",
    authDomain: "searching-document.firebaseapp.com",
    databaseURL: "https://searching-document.firebaseio.com",
    projectId: "searching-document",
    storageBucket: "searching-document.appspot.com",
    messagingSenderId: "470917742736",
    appId: "1:470917742736:web:dfc0dab1d2776e9da4d815"
};
firebase.initializeApp(firebaseConfig);

export const database	= firebase.database();
export default firebase;