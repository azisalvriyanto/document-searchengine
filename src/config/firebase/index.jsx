import firebase from "firebase/app";
import "firebase/database";

const firebaseConfig = {
  apiKey: "AIzaSyBQYq3rMPv-toWljFbVBaRw4h0wl1dM7d4",
  authDomain: "searching-document.firebaseapp.com",
  databaseURL: "https://searching-document.firebaseio.com",
  projectId: "searching-document",
  storageBucket: "searching-document.appspot.com",
  messagingSenderId: "470917742736",
  appId: "1:470917742736:web:dfc0dab1d2776e9da4d815"

  // apiKey: "AIzaSyD0xUC6W4rPccj1EMY_LBf6T84UFOustuk",
  // authDomain: "documentsearchengine-dummy.firebaseapp.com",
  // databaseURL: "https://documentsearchengine-dummy.firebaseio.com",
  // projectId: "documentsearchengine-dummy",
  // storageBucket: "documentsearchengine-dummy.appspot.com",
  // messagingSenderId: "321280172130",
  // appId: "1:321280172130:web:539812e78f76e31a93713b"
};
firebase.initializeApp(firebaseConfig);

export const database = firebase.database();
export default firebase;
