import { database }	from "../../firebase";

export const getState		= ()		=> (dispatch)	=> {
	return new Promise (async (resolve, reject) => {
		await database.ref(`state/database/`)
		.on("value", function(snapshot) {
			const data = snapshot.val();

			dispatch({
				type: "SET_DATABASE",
				value: data
			});

			console.log("    > Update database | done");
		});

		await database.ref(`state/calculation/`)
		.on("value", function(snapshot) {
			const data = snapshot.val();

			dispatch({
				type: "SET_CALCULATION",
				value: data
			});

			console.log("    > Update calculation | done");
			resolve({ status: true });
		});
	});
}

export const getFirebaseData	= (reference)		=> (dispatch)	=> {
	return new Promise (async (resolve, reject) => {
		await database.ref(`${reference}/`)
		.on("value", function(snapshot) {
			resolve({ status: true, data: snapshot.val() });
		});
	});
}

export const updateFirebaseData	= (reference, data)	=> (dispatch)	=> {
	return new Promise ((resolve, reject) => {
		database.ref(`${ reference }`)
		.set(data, (error) => {
			if(error) {
				reject({ status: false, message: error.message });
			}
			else {
				resolve({ status: true, data: data });
			}
		});
	})
}

export const getUpdateAt	= ()		=> (dispatch)	=> {
	return new Promise ((resolve, reject) => {
		database.ref(`update_at/`)
		.on("value", function(snapshot) {
			const data = snapshot.val();

			dispatch({
				type: "SET_UPDATEAT",
				value: data
			});

			resolve({ status: true, data: snapshot.val() });
		});
	});
}

export const updateUpdateAt	= (data)	=> (dispatch)	=> {
	return new Promise ((resolve, reject) => {
		database.ref(`update_at/`)
		.set(data, (error) => {
			if(error) {
				reject({ status: false, message: error.message });
			}
			else {
				resolve({ status: true, data: data });
			}
		});
	});
}