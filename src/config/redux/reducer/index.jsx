const initialStore = {
	state: {},
	update_at: ""
}

const reducer	= (state=initialStore, action) => {
	if (action.type === "SET_DATABASE") {
		return {
			...state,
			state: {
				...state.state,
				database: action.value
			}
		}
	}
	// if (action.type === "SET_DATABASE") {
	// 	return {
	// 		...state,
	// 		state: {
	// 			...state.state,
	// 			database: {
	// 				...state.state.database,
	// 				[action.document_id]: action.value
	// 			}
	// 		}
	// 	}
	// }
	else if (action.type === "SET_CALCULATION") {
		return {
			...state,
			state: {
				...state.state,
				calculation: action.value
			}
		}
	}
	else if (action.type === "SET_UPDATEAT") {
		return {
			...state,
			update_at: action.value
		}
	}
	else if (action.type === "SET_PS") {
		return {
			...state,
			state: {
				...state.state,
				vsm: {
					...state.state.vsm,
					document: {
						...state.state.document,
						ps: action.value
					}
				}
			}
		}
	}

	return state;
}

export default reducer;