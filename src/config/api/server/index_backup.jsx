export const site = `http://${
    window.location.hostname === "localhost" || window.location.hostname === "127.0.0.1"
    ?   window.location.hostname+"/document-searchengine-api/api"
    :   "167.114.33.205/document-searchengine-api/api"
}`;

// export const site = `${window.location.protocol}//${window.location.hostname}/document-searchengine-api/api`;

// export const site = `http://167.114.33.205/document-searchengine-api/api`;