import axios	from "axios";
import { site }	from "../../server"

const UpdateAt = (path) => {
	return new Promise((resolve, reject) => {
		axios.get(`${ site+path }`)
		.then(result => {
			resolve(result.data)
		}, error => {
			reject(error)
		});
	});
}

export default UpdateAt;