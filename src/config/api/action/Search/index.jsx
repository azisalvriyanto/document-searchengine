import axios	from "axios";
import { site }	from "../../server"

const Search = (path, keywords, orderby) => {
	return new Promise((resolve, reject) => {
		const data = new FormData();
        data.append("keywords", keywords);
        data.append("orderby", orderby);

		axios.post(`${ site+path }`, data)
		.then(result => {
			resolve(result.data)
		}, error => {
			reject(error)
		});
	});
}

export default Search;