import {
	Search,
	Document,
	UpdateAt,
}	from "../action";

const search	= (keywords, orderby="year")	=> Search		("/search", keywords, orderby);
const document	= ()			=> Document		("/documents");
const update_at	= ()			=> UpdateAt		("/update_at");

export const API = {
	search,
	document,
	update_at
}